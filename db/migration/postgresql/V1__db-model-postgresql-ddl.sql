CREATE TABLE bds.address (
    id_address bigserial NOT NULL,
    city character varying(45) NOT NULL,
    house_number integer NOT NULL,
    street character varying(45) NOT NULL,
    zip_code character varying(45),
    PRIMARY KEY (id_address))
	TABLESPACE "data1";

CREATE TABLE bds.person (
    id_person bigserial NOT NULL,
    birthdate date,
    email character varying(100) NOT NULL,
    given_name character varying(45) NOT NULL,
    family_name character varying(45) NOT NULL,
    nickname character varying(45) NOT NULL,
    pwd character varying(255) NOT NULL,
    id_address bigint,
    PRIMARY KEY (id_person),
    CONSTRAINT fksukp9r53qgth8ex065xwuw86t
        FOREIGN KEY (id_address)
        REFERENCES bds.address(id_address))
	TABLESPACE "data1";

CREATE TABLE bds.contact_type (
    id_contact_type serial NOT NULL,
    title character varying(45) NOT NULL,
    PRIMARY KEY (id_contact_type),
    UNIQUE (title))
	TABLESPACE "data1";

CREATE TABLE bds.contact (
    id_contact bigserial NOT NULL,
    contact character varying(45) NOT NULL,
    id_contact_type bigint,
    id_person bigint,
    PRIMARY KEY (id_contact),
    CONSTRAINT fksdofi6j6flua1it19rnx6xcvb 
        FOREIGN KEY (id_person)
        REFERENCES bds.person(id_person),
    CONSTRAINT fkx0wbmi3et03b7xwys8sa0d7
        FOREIGN KEY (id_contact_type)
        REFERENCES bds.contact_type(id_contact_type))
	TABLESPACE "data1";

CREATE TABLE bds.meeting (
    id_meeting bigserial NOT NULL,
    note character varying(1000),
    place character varying(200) NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    PRIMARY KEY (id_meeting))
	TABLESPACE "data1";

CREATE TABLE bds.person_has_meeting (
    id_person bigint NOT NULL,
    id_meeting bigint NOT NULL,
    is_organizer boolean NOT NULL,
    PRIMARY KEY (id_person, id_meeting),
    CONSTRAINT fk92pqvoxbjila4o02fp18rstq4 
        FOREIGN KEY (id_person)
        REFERENCES bds.person(id_person),
    CONSTRAINT fk92pqvoxbjila4o02fp18rstq5
        FOREIGN KEY (id_meeting)
        REFERENCES bds.meeting(id_meeting))
	TABLESPACE "data1";

CREATE TABLE bds.role (
    id_role SERIAL NOT NULL,
    role VARCHAR(45) NOT NULL,
    PRIMARY KEY (id_role))
	TABLESPACE "data1";

CREATE TABLE bds.person_has_role (
    id_person bigserial NOT NULL,
    id_role bigserial NOT NULL,
    started_at TIMESTAMP NOT NULL,
    ended_at TIMESTAMP,
    expiration_date TIMESTAMP,
    PRIMARY KEY (id_role, id_person),
    CONSTRAINT fk_person_has_role_person1
        FOREIGN KEY (id_person)
        REFERENCES bds.person (id_person)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_person_has_role_role1
        FOREIGN KEY (id_role)
        REFERENCES bds.role (id_role)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION)
	TABLESPACE "data1";

CREATE TABLE bds.relationship_type (
    id_relationship_type bigserial NOT NULL,
    title character varying(255) NOT NULL,
    PRIMARY KEY (id_relationship_type),
    UNIQUE (title))
	TABLESPACE "data1";

CREATE TABLE bds.relationship (
    id_relationship bigserial NOT NULL,
    note character varying(200),
    id_person1 bigint,
    id_person2 bigint,
    id_relationship_type bigint,
    PRIMARY KEY (id_relationship),
    CONSTRAINT fk3b7pcnfgfdblhublf8t8dvn9l
        FOREIGN KEY (id_relationship_type)
        REFERENCES bds.relationship_type(id_relationship_type)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fkahw6wujjfkm21yqfhar09k3fk
        FOREIGN KEY (id_person1) 
        REFERENCES bds.person(id_person),
    CONSTRAINT fkdkyv2jjonl0trwvbsnmw7wugr
        FOREIGN KEY (id_person2)
        REFERENCES bds.person(id_person))
	TABLESPACE "data1";

-- Extensions
CREATE EXTENSION citext;
CREATE EXTENSION pgcrypto;
CREATE EXTENSION unaccent;

-- Domains --
CREATE DOMAIN email AS citext
  CHECK (value ~ '^[a-zA-Z0-9.!#$%&''*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$');
  
-- Types --
CREATE TYPE gender_t AS ENUM ('Male', 'Female', 'Other');

-- Views --
CREATE VIEW bds.person_without_pwd AS
  SELECT p.id_person, p.birthdate, p.email, p.given_name, p.family_name, p.nickname, p.id_address
    FROM bds.person p;

-- Functions --
CREATE OR REPLACE FUNCTION bds.resetSequences() RETURNS SETOF BIGINT AS $$
DECLARE
	i text;
	rec record;
BEGIN
FOR i IN (
	SELECT 'SELECT SETVAL(' ||
	       quote_literal(quote_ident(PGT.schemaname) || '.' || quote_ident(S.relname)) ||
	       ', COALESCE(MAX(' ||quote_ident(C.attname)|| '), 1) ) FROM ' ||
	       quote_ident(PGT.schemaname)|| '.'||quote_ident(T.relname)|| ';'
	FROM pg_class AS S,
	     pg_depend AS D,
	     pg_class AS T,
	     pg_attribute AS C,
	     pg_tables AS PGT
	WHERE S.relkind = 'S'
	    AND S.oid = D.objid
	    AND D.refobjid = T.oid
	    AND D.refobjid = C.attrelid
	    AND D.refobjsubid = C.attnum
	    AND T.relname = PGT.tablename
	ORDER BY S.relname) loop	

	RETURN query
	EXECUTE i;	
END loop;
END;
$$ LANGUAGE plpgsql;
